set nocompatible

" ===Pathogen vim plugin 'package manager'===
" Set up for and installed pathogen, like so:
"    mkdir -p ~/.vim/autoload ~/.vim/bundle && \
"    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
" now, to add vim plugins, clone their git repo to ~/.vim/bundle
" and pathogen will do the rest
"call pathogen#infect()
"call pathogen#helptags()    "If you like to get crazy :) (this generates helptags for all plugins every time)
" Bind F9 to toggle tagbar on or off



" ===Vim-plug===
" Installed by runnig:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')

" Hilight line trailing whitespaces
Plug 'ntpeters/vim-better-whitespace'

Plug 'tpope/vim-sensible'

" Generate tag file for current file and display tag tree in sidebar
Plug 'majutsushi/tagbar'
nnoremap <silent> <F9> :TagbarToggle<CR>

"Plug 'xolox/vim-misc'
"Plug 'xolox/vim-easytags'

Plug 'szw/vim-tags'

" Fuzzy file selector
Plug 'ctrlpvim/ctrlp.vim'
" Ignore some files for ctrlp
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll|o)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

Plug 'godlygeek/tabular'

" Colorschemes
Plug 'chriskempson/base16-vim'


Plug 'airblade/vim-gitgutter'
" faster refresh for entered changes
set updatetime=250

Plug 'tpope/vim-fugitive'
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

Plug 'tpope/vim-commentary'

" use '-' to zoom out 1lvl at tim form current file in currnet split
" Remember that :Sex and :Lex is a thing too for new splits
Plug 'tpope/vim-vinegar'

" Distraction free fullscrean editor sesion
Plug 'junegunn/goyo.vim'

call plug#end()


"==setup for undo that is persistent after closing vim==
set undodir=~/.vim/undodir
set undofile
set undolevels=1000 "maximum number of changes that can be undone
set undoreload=10000 "maximum number lines to save for undo on a buffer reload
set hidden           " remember undo after quitting

"==display settings==
"set nowrap              " dont wrap lines
set wrap
set linebreak           " warp lines at sensible char, not just the last char that fits
set nolist              " list disables linebreak
"set scrolloff=2         " 2 lines above/below cursor when scrolling
set number              " show line numbers
set showmatch           " show matching bracket (briefly jump)
set showmode            " show mode in status bar (insert/replace/...)
set showcmd             " show typed command in status bar
set ruler               " show cursor position in status bar
set title               " show file in titlebar
set wildmenu            " completion with menu
"set lines=40           " default windows size. Bad for i3, guake, tilda etc.
"set columns=100
set gfn=Inconsolata\ Regular\ 10 " font for gvim
"set colorcolumn=81      " Higlight 81'th column
set bg=dark
set guioptions-=T
" preserve line indetation when word warping!!!
set breakindent

"==editor settings==
set tags=./tags;/       " make vim look for tags in parent dirs
set esckeys             " map missed escape sequences (enables keypad keys)
set ignorecase          " case insensitive searching
set smartcase           " but become case sensitive if you type uppercase characters
set smartindent         " smart auto indenting
set smarttab            " smart tab handling for indenting
set magic               " change the way backslashes are used in search patterns
set bs=indent,eol,start " Allow backspacing over everything in insert mode
set nobackup            " no backup~ files.
set dir=~/.vim/swapfiles     " save swap files in centralised directory
set autochdir
set fileformats=unix,dos,mac " support all three, in this order
set clipboard=unnamed,unnamedplus  " Use both linux clipbords(* and +)
set softtabstop=4
set tabstop=4           " number of spaces a tab counts for
set shiftwidth=4        " spaces for autoindents
set expandtab           " turn all tabs into spaces
set autoindent
set encoding=utf-8
set history=500         " keep 500 lines of command history
set mouse=a             " use mouse
set viminfo='20,\"500   " remember copy registers after quitting in the .viminfo file -- 20 jump links, regs up to 500 lines'
"set nofoldenable
set foldenable          " enable folding
set foldmethod=syntax
set foldnestmax=1       " 1 nested fold max
set foldlevelstart=1    " open folds by default
" Disable the beeping/flashing thing for vim and gvim
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=
" Set a nicer foldtext function
set foldtext=CustomFoldText()
fu! CustomFoldText()
    let line = getline(v:foldstart)
    if match( line, '^[ \t]*\(\/\*\|\/\/\)[*/\\]*[ \t]*$' ) == 0
        let initial = substitute( line, '^\([ \t]\)*\(\/\*\|\/\/\)\(.*\)', '\1\2', '' )
        let linenum = v:foldstart + 1
        while linenum < v:foldend
            let line = getline( linenum )
            let comment_content = substitute( line, '^\([ \t\/\*]*\)\(.*\)$', '\2', 'g' )
            if comment_content != ''
                break
            endif
            let linenum = linenum + 1
        endwhile
        let sub = initial . ' ' . comment_content
    else
        let sub = line
        let startbrace = substitute( line, '^.*{[ \t]*$', '{', 'g')
        if startbrace == '{'
            let line = getline(v:foldend)
            let endbrace = substitute( line, '^[ \t]*}\(.*\)$', '}', 'g')
            if endbrace == '}'
                let sub = sub.substitute( line, '^[ \t]*}\(.*\)$', '...}\1', 'g')
            endif
        endif
    endif

    let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0) + 4
    let foldSize = 1 + v:foldend - v:foldstart
    let foldSizeStr = " " . foldSize . " lines "
    let expansionString = repeat(" ", w - strwidth(foldSizeStr.sub))
    return sub . expansionString . foldSizeStr
endf

"==key maps==
"w!! allows to write files with sudo if vim is not run as root
cmap w!! w !sudo tee % > /dev/null
"map ; to :
nnoremap ; :
"map space to fold/unfold
"nnoremap <space> za
"window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
"tab navigation
"map <Tab> gt
"map <S-Tab> gT
" Stupid urxvt not changing Ctrl-PgUp and Ctrl-PgDown codes
nmap    <ESC>[5^    <C-PageUp>
nmap    <ESC>[6^    <C-PageDown>
"up/down line naviation over warped lines
vmap j gj
nmap j gj
vmap k gk
nmap k gk

" for ctags
"C-\ - Open the definition in a new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
"A-] - Open the definition in a vertical split
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" Insert current date and time with F2
map <F2> :r! date +"\%Y-\%m-\%d \%H:\%M:\%S \%w"<cr>

"==custom commands==
" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis


filetype on
filetype plugin on 

"==color settings (if terminal/gui supports it)==
if &t_Co > 2 || has("gui_running")
set t_Co=256
  syntax on          " enable colors
  set hlsearch       " highlight search (very useful!)
  set incsearch      " search incremently (search while typing)
  set cursorline
  colorscheme wombat256
  "colorscheme zellner
  hi LineNr guibg=grey30
endif

"==file type specific settings==
if has("autocmd")
  " smartindent for python
  autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class 

  " Pylint
  " autocmd FileType python compiler pylint

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 80 characters.
  " autocmd FileType text setlocal textwidth=80

  " Always jump to the last known cursor position. 
  " Don't do it when the position is invalid or when inside
  " an event handler (happens when dropping a file on gvim). 
  autocmd BufReadPost * 
    \ if line("'\"") > 0 && line("'\"") <= line("$") | 
    \   exe "normal g`\"" | 
    \ endif
  augroup END

  augroup filetypedetect
    au! BufRead,BufNewFile *.nc set filetype=nc
  augroup END

endif " has("autocmd")
